## 就一个 div，然后里面就给你几个字，然后你要让他垂直居中，你怎么实现它？

```css
#root {
  line-height: 200px; // 垂直
  text-align: center;
}

#root {
  /* flex 主轴一般都是水平的  */
  display: flex;
  align-items: center; // 垂直
  justify-content: center; // 水平
}
#root {
  display: table-cell; // 垂直
  vertical-align: middle; // 垂直
  text-align: center;
}
```

## 组件通讯

    1.通过 props
    2.redux
    3.useContext

## http 的一些问题

    1xx (Informational): 收到请求，正在处理

    2xx (Successful): 该请求已成功收到，理解并接受
    	200：成功

    3xx (Redirection): 重定向
    	301：永久移动
    	302：历史移动

    4xx (Client Error): 该请求包含错误的语法或不能为完成
    	400:请求参数错误

    5xx (Server Error): 服务器错误

## 还有 react hooks 问的都是很偏的

1.  useEffect 代替了 componentDidMount componentDidUpdate componentWillUnmount

    ```js
    // 当我们指定第二个参数为空数组就可以代替我们类组件中的componentDidMount
    useEffect(() => {
      console.log("Did mount!");
    }, []);

    // 当我们第二个参数监听一个状态的时候代替componentDidUpdate
    useEffect(() => {
      console.log("Did update!");
    }, [a]);

    // 当我们不指定第二个参数的同时并在第一个参数中返回一个函数就可以代替我们类组件中的componentWillUnmount
    // 用于清理 副作用 eg:清除定时器
    useEffect(() => {
      return () => {
        console.log("will unmount");
      };
    });
    ```

2.  useCallback、useMemo
    useCallback 返回的是一个 memoized（缓存）函数，在依赖不变的情况下,多次定义的时候,返回的值是相同的。
    useMemo 计算结果是 return 回来的值，通常用于缓存计算结果的值

    useMemo 和 useCallback 的共同点：
    接收的参数都是一样的，第一个是回调函数，第二个是依赖的数据
    它们都是当依赖的数据发生变化时才会重新计算结果，起到了缓存作用

    useMemo 和 useCallback 的区别：
    useMemo 计算结果是 return 回来的值，通常用于缓存计算结果的值
    useCallback 计算结果是一个函数，通常用于缓存函数

3.  useRef
    useRef 返回一个可变的 ref 对象，其.current 属性初始化为传递的参数（initialValue）。返回的对象将在组件的整个生存期内保持。
    简单来说，useRef 就像一个储物箱，你可以随意存放任何东西，再次渲染时它会去储物箱找，createRef 每次渲染都会返回一个新的引用，而 useRef 每次都会返回相同的引用。

## spa(单页面应用) 路由跳转原理

    核心原理解析：
    	路由描述了 URL 与 UI 之间的映射关系，这种映射是单向的，即 URL 变化引起 UI 更新（无需刷新页面）。前端路由最主要的展示方式有 2 种：
    		1.带有 hash 的前端路由：地址栏 URL 中有 #，即 hash 值，不好看，但兼容性高。
    		2.不带 hash 的前端路由：地址栏 URL 中没有 #，好看，但部分浏览器不支持，还需要后端服务器支持。

## 自我介绍

    你好，我是吕刻明现就职于 杭州来赞宝电子商务有限公司 担任前端开发。
    刚开始负责的是 wms仓库管理系统的出库模块，主要包括 出库单管理、订单打包、称重等一系列出库流程，提供仓库人员的出库效率

    目前负责公司 seaseller 和 gxb 系统的维护开发，这两个系统使用umi框架。
    seaseller 是公司的erp系统，主要用于公司全部资料数据的统计，例如财务的汇总、用户中心的权限等。
    我主要负责的是财务模块，为财务提供数据查询、校对等
    gxb是一个tms系统，该项目基本是我一个人维护开发的，主要用于给 公司合作的服务商提供物流渠道、包裹查询以及费用的报价、结算等

业务
lazmore 基本业务订单：通过授权把该账号下 不同平台（eg：京东、虾皮、lazada）的店铺数据（订单信息）全部统一到 lazmore 系统。
然后就可以不用一个个平台去看订单信息了

ss
干线运费（头程运费）是国内运输，是公司自己的物流。
尾程运费 是国外运输，是其他公司提供的物流。

wms
一单一品：一个订单只有一个 sku（一个订单中只有一种商品）
一单多品：一个订单多个 sku（一个订单中有多种商品）
波次：一个波次中可以存在任意数量的订单（订单数量>=1）
容器：一个容器存放一个波次，单品打包完结后或者是在多品播种完成后容器就释放了（意味着可以容器可以供后续的波次使用），容器可以反复使用。
播种墙：在多品播种中使用，播种墙可以有多个格子（每个格子都有自己的格子号），并且一个格子对应一个 一单多品的订单，与容器类似当该波次的多品打包完成之后该播种墙释放。

wms 业务 主要就是入库和出库
入库 ：包裹到仓库了 -> 先是登记包裹到货 -> 然后是去收货（收货的时候可能会有异常货品） -> 把收货完成的货品用上架车放到仓库里面（后端有自己的规则去判断放到哪里），这里会记录货品是放到仓库的具体位置。
出库 ： 1.新增一条出库单，这个时候需要选择货主（不同货主有不一样的货品）、承运商、物流单号（系统内 自己定义的） 2.然后系统会有一个定时任务去生成 该出库单对应的波次 3.通过波次编码，找到对应的波次，确定该波次的拣货人、容器号（只有状态为待拣货的波次，才能去拣货。） 4.去容器拣货，然后找到对应的容器，进行拣货。 5.订单打包，分为两种情况：一单一品和一单多品
a.一单一品：进入单品打包输入容器号，然后可以打包容器号内的 sku。当打包完成之后，容器就释放了。该波次中的所有订单状态为已复核。
b.一单多品：分为两个步骤先是多品播种，当多品播种完成之后，再去多品打包。
i）多品播种：输入对应的容器号，然后在输入播种墙（找一个未占用的播种墙即可，如果找不到未占用的，建议直接新增播种墙）。
ii）多品打包：输入对应的格子号，进行打包该格子中的商品。当一个播种墙中的全部格子都打包完成之后，该播种墙释放。该波次中的所有订单状态为已复核。

          6. 称重：每个订单都有对应的快递单号（运单号），已复核的订单才能去称重。先输入运单号，拿到该订单的信息，然后在输入该订单的重量。完成称重后，订单状态为已发运。

gxb：其实是一个 tms 系统，只是不大完善 ，主要业务 1.物流服务商管理 
服务商计费，比如说首重、续重；计费方式：体积、重量 、 计费区间（eg： 1-10 10 元 10-20 20 元）；有始发仓、目的地/仓
就是服务商多了一个结算，结算里面有 结算帐期（日结、月结、周结）
与其相关的是 出账条件根据轨迹状态（快递已揽收、签收、拒收）、结算费用的类型（运费、退件费、cod 手续费）
cod 是指到货付款

    2.物流渠道管理
      管理渠道类型（头程、尾程、全程），目的地、参考时效（到货需要的时间）
      渠道有自己的报价计费方式，比如说首重、续重；计费方式：体积、重量 、 计费区间（eg： 1-10 10元 10-20 20元）；有始发仓、目的地/仓

    3.包裹管理：
      查看包裹详细信息： 单号（gxb单号系统自己的单号 外部单号服务商给的单号）、状态（装箱、仓库作业中、快递提货）
          收件人、发件人、商品信息、订单的轨迹

    4.用户管理

    4.财务管理
      查看用户账单

seaseller 财务技术难点：
具体问题 就是财务查询数据量大，然后超过接口响应时间。利用 webSocket 做一个长连接进行处理。
然后跟后端约定好一个 status 表示是否完成数据查询，eg 1 建立连接 2 还在查询数据 3 查询完毕
当查询完毕的时候 关闭 webSocket

```js
import SockJS from "sockjs-client";
let sock = null;
const createWebSocket = (obj) => {
  let url =
    `${process.env.apiUrl}/websocket/erp?myToken=` + localStorage.myToken;
  // let url=  "http://10.10.10.81:9820/erp?myToken=f2c20bbc-2634-436e-86dc-658134f2575d"
  //let url="http://10.10.10.81:9820/erp?myToken=bc87655e-5516-4807-99cb-b0821c8eebb4"
  sock = new SockJS(url);
  sock.onopen = () => {
    //if(sock.readyState === 0){//初始化未完成}
    sock.send("sub,erp");
  };
  sock.onmessage = (e) => {
    let messageData = e.data.split(",");
    messageData = messageData.slice(2, messageData.length).join(",");
    messageData = JSON.parse(messageData);
    let status = ((messageData.body || {}).data || {}).status;
    let data = (messageData.body || {}).data;
    if (status == 1) {
      let params = {
        ns: "erp.ajax",
        body: obj.body,
      };
      let msg = "msg,erp," + JSON.stringify(params);
      sock.send(msg);
      console.log("createWebSocket>>>>>>>>>>>", msg);
      obj.success && obj.success(data);
    } else if (status == 2) {
      obj.success && obj.success(data);
    } else if (status == 3) {
      sock.close();
      obj.success && obj.success(data);
    }
  };
  sock.onclose = (e) => {
    // console.log('close')
    // console.log(e)
    obj.fail && obj.fail(e);
  };
  window.sock = sock;
};
let closeWebSocket = () => {
  sock.close();
};

export { createWebSocket, closeWebSocket };

getSearchWebsocket = (searchParams) => {
  let obj = {};
  obj.body = {
    method: "POST",
    url: "/lzdTransaction/detail/search",
    data: searchParams,
  };
  obj.success = (data) => {
    if (data.status == 3) {
      const { dispatch } = this.props;
      dispatch({
        type: "lazRecon/searchByCondition",
        payload: {
          data: data,
        },
      });
      this.setState({
        loadingDialogVisible: false,
        progressPercent: ((data.progress || 0) * 100).toFixed(2),
      });
    }
    this.setState({
      loadingDialogVisible: true,
      progressPercent: ((data.progress || 0) * 100).toFixed(2),
    });
  };
  obj.fail = (e) => {
    e.code == 1000 ? "" : messageError(e.reason);
    this.setState({
      loadingDialogVisible: false,
      loading: false,
    });
  };
  createWebSocket(obj);
};
```
