/**
 * @param {number[]} nums
 * @return {number[]}
 */
// 1685. 有序数组中差绝对值之和
/**
 * 给你一个 非递减 有序整数数组 nums 。
 * 请你建立并返回一个整数数组 result，它跟 nums 长度相同，且result[i] 等于 nums[i] 与数组中所有其他元素差的绝对值之和。
 * 换句话说， result[i] 等于 sum(|nums[i]-nums[j]|) ，其中 0 <= j < nums.length 且 j != i （下标从 0 开始）。
 * 
 * 
 * 输入：nums = [2,3,5]
 * 输出：[4,3,5]
 * 解释：假设数组下标从 0 开始，那么
 * result[0] = |2-2| + |2-3| + |2-5| = 0 + 1 + 3 = 4，
 * result[1] = |3-2| + |3-3| + |3-5| = 1 + 0 + 2 = 3，
 * result[2] = |5-2| + |5-3| + |5-5| = 3 + 2 + 0 = 5。
 * 
 * 
 * 输入：nums = [1,4,6,8,10]  sum = 29 ; len = 5
 * 输出：[24,15,13,15,21]
 * 
 * 找规律(题目给的非递减很重要)
 * ( 29-0 ) - 5*1 + 0*1 - ( 0 ) = 24
 * ( 29-1 ) - 4*4 + 1*4 - ( 1 ) = 15 
 * ( 29-1-4 ) - 3*6 + 2*6 - (1+4) = 13
 * ( 29-1-4-6 ) - 2*8 + 3*8 - (1+4+6) = 15
 * ( 29-1-4-6-8 ) - 1*10 + 4*10 - (1+4+6+8)  = 21
 */
var getSumAbsoluteDifferences = function (nums) {
    let ans = [], n = nums.length, leftSum = 0

    let rightSum = nums.reduce((pre, cur) => pre + cur, 0)

    for (let i = 0; i < n; i++) {
        let temp = rightSum - (n - i) * nums[i] + (i * nums[i]) - leftSum
        rightSum -= nums[i]
        leftSum += nums[i]

        ans.push(temp)
    }

    return ans
};