/**
 * @param {number[]} nums
 * @return {number}
 * 假设数组 {nums} 的长度为 n。							假设 nums = [2, 7, 9, 3, 1]
 * 如果不偷窃最后一间房屋，则偷窃房屋的下标范围是 [0,n−2]	 实际偷窃范围： [2, 7, 9, 3]
 * 如果不偷窃第一间房屋，则偷窃房屋的下标范围是 [1,n−1]		 实际偷窃范围：[7, 9, 3, 1]
 */
// 213. 打家劫舍 II
/**
 * 1 <= nums.length <= 100
 * 
 * 输入：nums = [1,2,3,1]
 * 输出：4
 * 解释：你可以先偷窃 1 号房屋（金额 = 1），然后偷窃 3 号房屋（金额 = 3）。
     偷窃到的最高金额 = 1 + 3 = 4 。
 */
var rob = function (nums) {
	if (nums.length === 1) return nums[0];
	if (nums.length === 2) return Math.max(nums[0], nums[1]);

	/**
	 * @param {*} nums 目标数组
	 * @param {*} i 开始下标
	 * @param {*} j 结束下标
	 * @returns
	 */
	const robRange = function (nums, i, j) {
		// 初始化 first：第一天的最大金额	second：第二天的最大金额
		let first = nums[i], second = Math.max(nums[i], nums[i + 1]);

		// 循环遍历时  first：第n-2天的最大金额	second：第n-1天的最大金额
		for (let start = i + 2; start <= j; start++) {
			let temp = first + nums[start]; // 第n天 + 第n-2天的最大金额

			first = second; // 更新 第n-2天的最大金额

			second = Math.max(second, temp); // 拿到第n天的最大金额
		}

		return second;
	};

	console.log(
		Math.max(
			robRange(nums, 0, nums.length - 2),
			robRange(nums, 1, nums.length - 1)
		)
	);

	return Math.max(
		robRange(nums, 0, nums.length - 2),
		robRange(nums, 1, nums.length - 1)
	)
};