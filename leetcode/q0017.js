/**
 * @param {string} digits
 * @return {string[]}
 * 排列组合的回溯：
 *  1.用map保存数字对应的字母（也可能是其他的映射情况）
 *  2.从第一个数字开始遍历，取到对应字符串，然后从第二个数字，取到对应字符串，第三个数字，取到对应字符串...
 *  3.数字遍历完了，将拼接好的字符串str加入结果数组res
 *  4.回溯，修改最后一个数字对应的字母,使其回到上一个状态
 *  5.重复2-4过程，直到遍历完 对应字符串
 */
// 17. 电话号码的字母组合
/**
 * 给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。
 * 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。
 * digits[i] 是范围 ['2', '9'] 的一个数字。
 */

// 用于电话号码的映射
const map = {
	2: "abc",
	3: "def",
	4: "ghi",
	5: "jkl",
	6: "mno",
	7: "pqrs",
	8: "tuv",
	9: "wxyz",
};

var letterCombinations1 = function (digits) {
	let ans = [];
	if (digits.length === 0) return ans;

	const dfs = (path, digits) => {
		// 跳出回溯的条件，digits数字字符串为空，表明拼接完了
		if (digits.length === 0) {
			ans.push(path);
			return;
		}

		// 获取数字对应的字符串，由于每一个digits都是被修剪过的
		// 所以只要拿到digits的第一个，就是我们下一个需要拼接的数字
		let numStr = map[digits[0]];
		for (let i = 0; i < numStr.length; i++) {
			// 拼接英文字符串
			path += numStr[i];

			// 进行下一个状态的拼接，记得要修剪digits，使其第一个数字就是我们下一个需要拼接的数字
			dfs(path, digits.slice(1));

			// 回溯到上一个状态
			path = path.slice(0, -1);
		}
	};
	dfs("", digits);

	console.log(ans);
	return ans;
};

var letterCombinations2 = function (digits) {
	let ans = [];
	if (digits.length === 0) return ans;

	const dfs = (path, index) => {
		// 跳出循环
		if (index === digits.length) {
			ans.push(path);
			return;
		}

		// 获取数字对应的字符串，index 就是我们当前需要拼接数字的下标
		let numStr = map[digits[index]];
		for (let i = 0; i < numStr.length; i++) {
			// 拼接英文字符串
			path += numStr[i];	 

			// 进行下一个状态的拼接，index + 1 拿到digits下一个需要拼接的数字
			dfs(path, index + 1);

			// 回溯到上一个状态
			path = path.slice(0, -1);
		}
	};
	dfs("", 0);

	console.log(ans);
	return ans;
};