
/**
 * 其实这道题是要我们将数组中值相等的索引放到一个分组中。
 * 数组的索引对应的是用户的 ID，用上面的代码来举例。
 * 
 * 输入：groupSizes = [2,1,3,3,3,2]
 * 输出：[[1],[0,5],[2,3,4]]
 * 
 * 例如用户 0 所在组大小为 2，也就是说这个组有 2 个人。然后索引 5，也就是用户 5 所在的用户组也是 2。
 * 说明用户 0 和用户 5 是在一个组。索引 1，也就是用户 1 所在的组为 1，只有它自己。
 * 用户 2 3 4 为一组，因为组大小为 3，有 3 个人。
 * 
 * 这样就能看出来有 3 个组了，所以答案是 [[1],[0,5],[2,3,4]]  (里面子数组的顺序可以是任意的)
 * 
 * 示例：
 * 输入：groupSizes = [3,3,3,3,3,1,3]
 * 输出：[[5],[0,1,2],[3,4,6]]
 */

/**
 * @param {number[]} groupSizes
 * @return {number[][]}
 * 
 * 解题的思路主要是利用 map存以下信息  
 * 	key-groupSizes[i]——当前所在组的人数 ； 
 *  value——{ cnt , arr } —— 当前所在组 cnt-剩余人数，arr-人员的id
 * 当cnt为0的时候，就去清空map中对应的键值对，同时把 arr 放入 ans中
 */
var groupThePeople = function (groupSizes) {
    let ans = []

    // key-s[i] , value-{ cnt , arr }
    let map = new Map()

    for (let i = 0; i < groupSizes.length; i++) {
        if (map.has(groupSizes[i])) {
            let { cnt, arr } = map.get(groupSizes[i])
            if (cnt > 0) {
                cnt--
                map.get(groupSizes[i]).cnt-- // 注意更新map中的cnt
                arr.push(i)  // 这里利用js数据引用
            }
            if (cnt == 0) {
                ans.push(arr)
                map.delete(groupSizes[i])
            }
        } else {
            if (groupSizes[i] == 1) {
                ans.push([i])
            } else {
                map.set(groupSizes[i], {
                    cnt: groupSizes[i] - 1,
                    arr: [i],
                })
            }
        }
    }

    return ans
};