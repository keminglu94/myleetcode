/**
 * @param {number} x
 * @return {boolean}
 */
// 9. 回文数
/**
 * 输入：x = -121
 * 输出：false
 * 解释：从左向右读, 为 -121 。 从右向左读, 为 121- 。因此它不是一个回文数。
 */
var isPalindrome = function (x) {
	if (x < 0) return false;
	let temp = x, res = 0;
	while (temp > 0) {
		let y = temp % 10;

		res = res * 10 + y;

		temp = Math.floor(temp / 10);
	}
	return res === x;
};