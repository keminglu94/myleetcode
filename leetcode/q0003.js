/**
 * @param {string} s
 * @return {number}
 * 利用 map 记录每一字符出现的下标。
 * 如果出现相同的字符，那么就要去更新left（无重复字符的左边界），要拿到最新的left，也就是该字符最近一次出现的下标。
 * 每次都要计算无重复字符的长度，和记录字符出现的下标。
 */
// 3. 无重复字符的最长子串
// 给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
var lengthOfLongestSubstring = function (s) {
	let map = new Map(), ans = 0, left = 0;
	for (let i = 0; i < s.length; i++) {
		if (map.has(s[i])) {
			left = Math.max(left, map.get(s[i]));
		}
		ans = Math.max(ans, i - left + 1);
		map.set(s[i], i + 1);
	}
	console.log(ans);
	return ans;
};