/**
 * @param {number[]} nums
 * @return {number}
 * 利用 set 元素不重复以及哈希（建值对查找更快，时间复杂度可以达到O(1) ）的特性
 * 往上还是往下查找（例子： [100,4,200,1,3,2]  ）：
 * 1. 往上查找，只需要从1（最小值）开始找一遍即可。
 * 2. 往下查找，我们需要从4开始找一遍、遇到3和2也需要找一遍，浪费了很多次的查找
 */
// 128. 最长连续序列
/**
 * 给定一个未排序的整数数组 nums ，找出数字连续的最长序列（不要求序列元素在原数组中连续）的长度。
 * 请你设计并实现时间复杂度为 O(n) 的算法解决此问题。
 * 
 * 输入：nums = [100,4,200,1,3,2]
 * 输出：4
 * 解释：最长数字连续序列是 [1, 2, 3, 4]。它的长度为 4。
 */
var longestConsecutive = function (nums) {
	// set 不会出现重复的元素
	let set = new Set(nums), ans = 0;

	for (num of set) {
		// 从数字连续序列的最小值开始寻找
		// 如果不存在 num-1 那么，num肯定是该数字连续序列的最小值
		if (!set.has(num - 1)) {
			// 我们就只需要一个个的往上寻找即可，
			let temp = 1, curNum = num + 1;
			while (set.has(curNum)) {
				temp++;
				curNum++;
			}
			ans = Math.max(ans, temp);
		}
	}

	console.log(ans);
	return ans;
};

/**
 * 思路：
 * 1. 题目要求是连续的序列
 * 2. 先把数组存到set中
 * 3. 然后我们开始计算 set 中连续的序列
 * 4. 计算左边就是 left-- ， 计算右边就是 right++ ，同时更新 temp 的大小
 * 5. 需要注意的点就是计算之后需要删除 set 中对应的数字，这样可以避免重复的计算
 */
var longestConsecutive2 = function (nums) {
    let set = new Set(nums), ans = 0;

    for (const num of nums) {
        let temp = 0, left = num, right = num + 1;

        while (set.has(left)) {
            set.delete(left);
            temp++;
            left--;
        }
        while (set.has(right)) {
            set.delete(right);
            temp++;
            right++;
        }

        ans = Math.max(ans, temp);
    }
    console.log(ans);
    return ans;
};