/**
 * @param {number[]} nums
 * @return {number}
 *  不能连续偷钱，中间一定要间隔一个，由此得出 第n天的最大金额，由以下两种情况比较得出：
 * 		第n-1天最大金额 	和 	第n-2的天最大金额+第n天的金额
 * 		dp[n] = Math.max( dp[n - 1], dp[n - 2] + nums[n] );
 */
// 198. 打家劫舍
/**
 * 每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统。
 * 如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。
 * 给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额。
 * 
 * 1 <= nums.length <= 100
 * 0 <= nums[i] <= 400
 * 
 * 输入：[2,7,9,3,1]
 * 输出：12
 * 解释：偷窃 1 号房屋 (金额 = 2), 偷窃 3 号房屋 (金额 = 9)，接着偷窃 5 号房屋 (金额 = 1)。
 * 		偷窃到的最高金额 = 2 + 9 + 1 = 12 。
 */
// 未优化空间
var rob1 = function (nums) {
	if (nums.length === 1) return nums[0];
	const n = nums.length;
	let dp = new Array(n);

	// 初始化 第一天和第二天的最大金额
	dp[0] = nums[0];
	dp[1] = Math.max(nums[1], nums[0]);

	for (let i = 2; i < n; i++) {
		dp[i] = Math.max(dp[i - 2] + nums[i], dp[i - 1]);
	}

	console.log(dp);
	return dp[n - 1];
};

var rob2 = function (nums) {
	if (nums.length === 1) return nums[0];
	// 初始化 first：第一天的最大金额	second：第二天的最大金额
	let first = nums[0], second = Math.max(nums[0], nums[1]);

	// 循环遍历时  first：第n-2天的最大金额	second：第n-1天的最大金额
	for (let i = 2; i < nums.length; i++) {
		let temp = first + nums[i]; // 第n天 + 第n-2天的最大金额

		first = second;	// 更新 第n-2天的最大金额 

		second = Math.max(second, temp); // 拿到第n天的最大金额
	}

	console.log(second);
	return second
};