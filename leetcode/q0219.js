/**
 * @param {number[]} nums
 * @param {number} k
 * @return {boolean}
 */
// 219. 存在重复元素 II
/**
 * 给你一个整数数组 nums 和一个整数 k ，判断数组中是否存在两个 不同的索引 i 和 j 
 * 满足 nums[i] == nums[j] 且 abs(i - j) <= k 。
 * 如果存在，返回 true ；否则，返回 false 。
 */
var containsNearbyDuplicate = function (nums, k) {
	let map = new Map(), ans = false;

	for (let i = 0; i < nums.length; i++) {
		if (map.has(nums[i]) && i - map.get(nums[i]) <= k) {
			console.log(true);
			return true;
		}
		map.set(nums[i], i);
	}
	
	console.log(ans);
	return ans;
};