/**
 * @param {number[][]} grid
 * @return {number}
 * 每次只能向下或者向右移动一步，所以上边界和左边界是没有选择的。
 * 只能加上 左一步或者上一步。 
 * 
 * 那么只需要比较 
 * 		上一步：grid[i - 1][j] 
 * 		左一步：grid[i][j - 1]
 */
// 64. 最小路径和
/**
 * 给定一个包含非负整数的 m x n 网格 grid ，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。
 * 说明：每次只能向下或者向右移动一步。
 */
var minPathSum1 = function (grid) {
	let y = grid.length;
	let x = grid[0].length;

	// 处理左边界
	for (let i = 1; i < y; i++) {
		grid[i][0] += grid[i - 1][0];
	}

	// 处理上边界
	for (let i = 1; i < x; i++) {
		grid[0][i] += grid[0][i - 1]
	}

	for (let i = 1; i < y; i++) {
		for (let j = 1; j < x; j++) {
			grid[i][j] += Math.min(grid[i - 1][j], grid[i][j - 1])
		}
	}

	console.log(grid);
	return grid[y - 1][x - 1]
};

var minPathSum2 = function (grid) {
	let y = grid.length;
	let x = grid[0].length;

	let dp = Array(x);
	for (let i = 0; i < y; i++) {
		for (let j = 0; j < x; j++) {
			if (i === 0 && j === 0) {
				// 初始化
				dp[j] = grid[i][j];
			} else if (i === 0) {
				// 处理上边界
				dp[j] = grid[i][j] + dp[j - 1];
			} else if (j === 0) {
				// 处理左边界
				dp[j] += grid[i][j];
			} else {
				dp[j] = Math.min(dp[j], dp[j - 1]) + grid[i][j];
			}
		}
		console.log(dp);
	}
	return dp[x - 1]
};