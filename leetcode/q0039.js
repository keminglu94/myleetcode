/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 * 该题回溯需要注意点就是 如何避免出现 [2,2,3] [2,3,2] [3,2,2]，以及 数组总和<target 如何继续进行递归
 * 	1.通过index来避免重复的情况 dfs(path, sum, index)，直接控制遍历 candidates 的初始位置，当遍历到第index个元素时，我们就从第index和元素开始寻找后面的情况
 *  2.分成三种情况 
 * 		2.1 数组总和 = target;	得到答案了，存到ans中，并return
 * 		2.2 数组总和 < target;	直接return，或者不往下寻找情况
 * 		2.3 数组总和 > target;	继续往下寻找情况
 */
// 39. 组合总和
/**
 * 给你一个 无重复元素 的整数数组 candidates 和一个目标整数 target ，找出 candidates 中可以使数字和为目标数 target 的 所有不同组合 ，并以列表形式返回。你可以按 任意顺序 返回这些组合。
 * candidates 中的 同一个 数字可以 无限制重复被选取 。如果至少一个数字的被选数量不同，则两种组合是不同的。 
 * 对于给定的输入，保证和为 target 的不同组合数少于 150 个。
 * 
 * 输入：candidates = [2,3,6,7], target = 7
 * 输出：[[2,2,3],[7]] // 注意 2 可以使用多次。
 */
var combinationSum1 = function (candidates, target) {
	let ans = [];

	const dfs = (path, sum, index) => {
		if (sum === target) {
			ans.push([...path]);
			return
		}

		for (let i = index; i < candidates.length; i++) {
			// 拿到
			sum += candidates[i];
			path.push(candidates[i]);

			// 进行剪枝
			if (sum <= target) {
				dfs(path, sum, i);
			}

			// 回到上一个状态
			sum -= candidates[i];
			path.pop();
		}
	};
	dfs([], 0, 0);

	console.log(ans);
	return ans;
};

/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 * 这里是根据 target 来判断 path 是否正确，其余思路同 combinationSum1
 */
// 39. 组合总和
var combinationSum2 = function (candidates, target) {
	let ans = [];

	const dfs = (path, index) => {
		if (target === 0) {
			ans.push([...path]);
			return;
		}

		for (let i = index; i < candidates.length; i++) {
			target -= candidates[i];
			path.push(candidates[i]);

			if (target >= 0) {
				dfs(path, i);
			}

			target += candidates[i];
			path.pop();
		}
	};
	dfs([], 0);

	console.log(ans);
	return ans;
};