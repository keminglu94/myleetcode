/**
 * @param {string} s
 * @return {boolean}
 * 利用队列先进先出的特性
 * 注意一下两种情况：
 * 		1 s = '['
 * 		2 s = ']'
 */
// 20. 有效的括号
/**
 * 有效字符串需满足：
 * 	左括号必须用相同类型的右括号闭合。
 * 	左括号必须以正确的顺序闭合。
 */
var isValid = function (s) {
	let queue = []; // js数组模拟队列，unshift入栈，shift出栈
	for (let i = 0; i < s.length; i++) {
		if (s[i] === "(" || s[i] === "[" || s[i] === "{") {
			queue.unshift(s[i]);
		} else {
			if (!queue[0]) return false; // 处理 s = ']' 的情况
			else {
				let left = queue[0], right = s[i];
				if (
					(left === "[" && right === "]") ||
					(left === "(" && right === ")") ||
					(left === "{" && right === "}")
				) {
					queue.shift();
				}
				else {
					return false;
				}
			}
		}
	}

	return queue.length === 0;  // 处理 s = '[' 的情况
};