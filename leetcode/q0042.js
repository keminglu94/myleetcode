/**
 * @param {number[]} height
 * @return {number}
 */
// 42. 接雨水
/**
 * 给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。
 * 
 *    			  +
 * 		  +		  + +    +
 *    +   + +   + + +  + +  +
 *  1 2 3 4 5 6 7 8 9 10 11 12
 * 
 * 输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
 * 输出：6
 * 解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。 
 */
var trap = function (height) {
	let i = 0,j = height.length - 1,ans = 0;
	let leftMax = 0,rightMax = 0;

	while (i < j) {
		// 每次都拿到 两边的最大值
		leftMax = Math.max(leftMax, height[i]);
		rightMax = Math.max(rightMax, height[j]);

		if (leftMax >= rightMax) {
			// 左边比右边高 我们就接右边的雨水
			ans += rightMax - height[j]
			j--
		} else {
			// 右边比左边高 我们就接左边的雨水
			ans += leftMax - height[i]
			i++
		}
	}

	console.log(ans);
	return ans;
};