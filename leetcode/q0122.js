/**
 * @param {number[]} prices
 * @return {number}
 * 贪心算法：
 * 	1.买入第一天的股票	temp = prices[0]
 *  2.如果第二天的价格比前一天高，卖出股票，并且在买入该股票。
 *  3.如果第二天的价格比前一天低，重置手上的股票。
 */
// 122. 买卖股票的最佳时机 II
/**
 * 给定一个数组 prices ，其中 prices[i] 是一支给定股票第 i 天的价格。
 * 设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
 * 注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
 */
var maxProfit = function (prices) {
	let ans = 0,
		temp = prices[0];

	for (let i = 1; i < prices.length; i++) {

		// 思路：
		// if (temp < prices[i]) {
		// 	ans += prices[i] - temp;
		// 	temp = prices[i];
		// } else {
		// 	temp = prices[i]
		// }
		
		// 简化版
		if (temp < prices[i]) {
			ans += prices[i] - temp;
		}
		temp = prices[i]
	}

	console.log(ans);
	return ans;
};