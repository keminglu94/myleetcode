/**
 * @param {number} x
 * @return {number}
 * 注意js的运算符
 */
// 7. 整数反转
// 如果反转后整数超过 32 位的有符号整数的范围 [−2^31,  2^31 − 1] ，就返回 0。
var reverse = function (x) {
	let ans = 0
	while (x !== 0) {
		let temp = x % 10
		ans = ans * 10 + temp
		// x = x/10 | 0
		x = ~~(x / 10)
		if (ans < Math.pow(-2, 31) || ans > Math.pow(2, 31) - 1) {
			return 0;
		}
	}
	return ans
};