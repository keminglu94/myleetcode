/**
 * @param {number} n
 * @return {string[]}
 * 1.明确跳出的回溯的条件，同时把该情况保存到res中
 * 2.明确剪枝的依据，避免出现错误的答案
 * 3.进行下一个分支时，更新左右边界条件以及情况的变化
 * 
 * 解题思路：
 *  1.可以先做出单纯的排列组合情况 如 getAllState()
 * 	2.然后我们在排列组合的基础上，进行剪枝 
 * 
 * ref https://leetcode-cn.com/problems/generate-parentheses/solution/pei-yang-chou-xiang-si-wei-hui-su-jie-fa-7dwu/
 */
// 22. 括号生成
// 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。
/**
 * 输入：n = 1
 * 输出：["()"]
 * 
 * 输入：n = 3
 * 输出：["((()))","(()())","(())()","()(())","()()()"]
 */
var generateParenthesis = function (n) {
	let res = []
	if (n <= 0) return res

	const dfs = (path, left, right) => {
		// 剪枝依据： 左右括号 的数量不能超过n，并且 左括号的数量一定要小于右括号的数量（括号要形成闭合）
		if (left > n || right > n || left < right) return

		// 跳出回溯的条件 并把情况加入到res中
		if (path.length === 2 * n) {
			res.push(path)
			return
		}

		dfs(path + "(", left + 1, right);	// 左括号 + 1，并更新左右括号数量
		dfs(path + ")", left, right + 1); 	// 右括号 + 1，并更新左右括号数量
	}

	dfs('', 0, 0) // 调用函数
	return res
};


/**
 * 
 * @param {number} n
 * @return {string[]}
 */
// 得到回溯所有 2*n 的结果，一共有 2的2*n次 情况，没有剪枝
var getAllState = function (n) {
	let res = [];
	if (n <= 0) return res;

	const dfs = (path, left, right) => {
		// 跳出回溯的条件 并把情况加入到res中
		if (path.length === 2 * n) {
			res.push(path);
			return;
		}
		dfs(path + "(", left + 1, right);	// 左括号 + 1
		dfs(path + ")", left, right + 1); 	// 右括号 + 1
	};
	dfs("", 0, 0); // 调用函数

	console.log(res);
	return res;
}