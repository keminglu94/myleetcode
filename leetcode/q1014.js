/**
 * @param {number[]} values
 * @return {number}
 * 智力题:  values[i] + values[j] + i - j 等价于 ( values[i] + i )+ ( values[j] - j )
 * 可以得出 values[j] - j 是一个不变的值，那么本题就变成了找 values[i] + i 的最大值
 */
// 1014. 最佳观光组合
/**
 * 给你一个正整数数组 values，其中 values[i] 表示第 i 个观光景点的评分，并且两个景点 i 和 j 之间的 距离 为 j - i。
 * 一对景点（i < j）组成的观光组合的得分为 values[i] + values[j] + i - j ，也就是景点的评分之和 减去 它们两者之间的距离。返回一对观光景点能取得的最高分。
 * 
 * 输入：values = [8,1,5,2,6]
 * 输出：11
 * 解释：i = 0, j = 2, values[i] + values[j] + i - j = 8 + 5 + 0 - 2 = 11
 */
var maxScoreSightseeingPair = function (values) {
	// max 是 values[i] + i 最大值，刚开始用 values[0] + 0 赋值
	let ans = 0, max = values[0] + 0;

	for (let j = 1; j < values.length; j++) {
		// 每一次都计算 ans，记得加上 values[j] - j
		ans = Math.max(ans, max + values[j] - j);
		// 维护 max（values[i] + i） 这个最大值
		max = Math.max(max, values[j] + j);
	}

	console.log(ans);
	return ans;
};