/**
 * @param {number[]} nums
 * @return {number}
 * 贪心，只要不是0，就往子数组加数字，计算乘积，遇到0就重置，从新的非0数开始
 * 那么遇到负数怎么办呢？
 * 答案是
 * 前缀算一遍，后缀算一遍
 * 如果是偶数个负数，那么算一遍就够了
 * 如果是奇数个负数，显然要么放弃最左边的负数及以左边的数（相当于只计算后缀积），要么放弃最右边的负数（相当于只计算前缀积）
 * 所以算俩遍，前缀后缀总有一个会遍历到 处理好0和负数的情况，问题也就迎刃而解
 */
// 152. 乘积最大子数组
/**
 * 给你一个整数数组 nums ，请你找出数组中乘积最大的连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。
 */
var maxProduct1 = function (nums) {
	// left 前缀积 ； right 后缀积
	let ans = nums[0], left = 0, right = 0, n = nums.length;

	for (let i = 0; i < n; i++) {
		//遇到0就重置子数组前缀积和后缀积
		if (left === 0) {
			left = nums[i];
		} else {
			left *= nums[i];
		}

		if (right === 0) {
			right = nums[n - 1 - i];
		} else {
			right *= nums[n - 1 - i];
		}

		// 拿到最大值
		ans = Math.max(left, right, ans);
	}

	console.log(ans);
	return ans;
};

/**
 * 动态规划
 * 如果当前位置是一个负数的话，我们希望它之前一个位置结尾的某个段的积也是个负数，这样就可以负负得正，并且我们希望这个积尽可能「负得更多」，即尽可能小。
 * 如果当前位置是一个正数的话，我们更希望它之前一个位置结尾的某个段的积也是个正数，并且希望它尽可能地大。
 * 所以需要 两个dp分别记录 最大值和最小值，由于 负数*正数 = 负数 ，所以在比较的时候 要同时比较 dpMax[i-1]*nums[i], dpMin[i-1]*nums[i] ,nums[i]
 * 
 * dpMax[i] = Math.max( dpMax[i-1]*nums[i], dpMin[i-1]*nums[i] ,nums[i] )
 * dpMin[i] = Math.min( dpMax[i-1]*nums[i], dpMin[i-1]*nums[i] ,nums[i] )
 */
var maxProduct2 = function (nums) {
	let dpMax = [...nums], dpMin = [...nums]
	let ans = dpMax[0]

	for (let i = 1; i < nums.length; i++) {
		dpMax[i] = Math.max(dpMax[i - 1] * nums[i], dpMin[i - 1] * nums[i], nums[i])
		dpMin[i] = Math.min(dpMax[i - 1] * nums[i], dpMin[i - 1] * nums[i], nums[i])

		ans = Math.max(ans, dpMax[i])
	}

	console.log(ans);
	return ans
};

// 优化空间 ，可以和 maxProduct1 比较，发现还是有很多相似的地方
var maxProduct3 = function (nums) {
	let ans = nums[0], min = nums[0], max = nums[0];

	for (let i = 1; i < nums.length; i++) {
		let tempMax = max * nums[i], tempMin = min * nums[i]

		max = Math.max(tempMax, tempMin, nums[i]);
		min = Math.min(tempMax, tempMin, nums[i]);

		ans = Math.max(ans, max);
	}

	console.log(ans);
	return ans;
};
