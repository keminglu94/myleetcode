/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 */
// 67. 二进制求和
// 参照 q0415
var addBinary = function (a, b) {
	let ans = "", i = a.length - 1, j = b.length - 1, carry = 0;

	while (i >= 0 || j >= 0) {
		let x = i >= 0 ? a[i] - "0" : 0;
		let y = j >= 0 ? b[j] - "0" : 0;
		let temp = (x + y + carry) % 2;
		carry = Math.floor((x + y + carry) / 2);
		ans += temp; // js  Number类型与String类型 相加的结果为 String类型
		i--;
		j--;
	}

	if (carry > 0) {
		ans += carry;
	}

	// 站在api的肩膀上
	console.log(ans.split("").reverse().join(""));
	return ans.split("").reverse().join("");
};