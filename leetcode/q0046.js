/**
 * @param {number[]} nums
 * @return {number[][]}
 * 回溯题目：
 * 	1.可以先枚举出全部的情况，这里建议换一个树状图 例如下图：
 * 				         []
 * 		  1			      2				   3
 *   1    2    3	 1    2    3      1    2    3
 *  123  123  123   123  123  123	 123  123  123
 * 
 *  2.再根据的题目的意思进行剪枝，把 1-1-1、1-1-2、1-1-3 .... 这些不符合题意的分支剪去
 */
// 46. 全排列
/**
 * 定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。
 * 输入：nums = [1,2,3]
 * 输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 */
var permute = function (nums) {
	let ans = [];
	if (nums.length === 0) return ans;

	const dfs = (path) => {
		if (path.length === nums.length) {
			ans.push([...path]); // js里面数组是引用类型，如果不理解可以补一补js数据存储方式了
			return;
		}

		for (let i = 0; i < nums.length; i++) {
			// 剪枝，数据是不允许重复的
			if (!path.includes(nums[i])) {
				// 在数据不重复的情况下 进行下一种情况的拼接
				path.push(nums[i]);

				dfs(path);

				path.pop();
			}
		}
	};
	dfs([]);

	console.log(ans);
	return ans;
};

/**
 * @param {number[]} nums
 * @return {number[][]}
 * 拿到全部的情况
 */
var getAllState = function (nums) {
	let ans = [];
	if (nums.length === 0) return ans;

	const dfs = (path) => {
		if (path.length === nums.length) {
			ans.push([...path]); // js里面数组是引用类型
			return;
		}

		for (let i = 0; i < nums.length; i++) {
			path.push(nums[i]);

			dfs(path);

			path.pop();
		}
	};
	dfs([]);

	console.log(ans);
	return ans;
};