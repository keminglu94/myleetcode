/**
 * @param {number[]} temperatures
 * @return {number[]}
 * 1.利用栈的思想，我们只要比较栈顶元素与当前元素
 * 2.维护栈顶元素：需要把 弹出所有小于当前温度的元素。利用while
 * 3.每次都都把元素存入栈中
 * 4.记得反向遍历、以及处理温度相同的情况
 */
// 739. 每日温度
/**
 * 请根据每日 气温 列表 temperatures ，请计算在每一天需要等几天才会有更高的温度。
 * 如果气温在这之后都不会升高，请在该位置用 0 来代替。
 *
 * 输入: temperatures = [73,74,75,71,69,72,76,73]
 * 输出: [1,1,4,2,1,1,0,0]
 * 1 <= temperatures.length <= 105
 * 30 <= temperatures[i] <= 100
 */
var dailyTemperatures = function (temperatures) {
	let ans = Array(temperatures.length).fill(0);
	let stack = [];

	for (let i = temperatures.length - 1; i >= 0; i--) {
		// 只要拿到 比当前温度大 并且是最近的下标
		// 所以我们需要 删除小于当前温度小的数据
		while (stack.length > 0 && stack[0].temperature <= temperatures[i]) {
			stack.shift();
		}
		// 拿到 比当前温度大 并且是最近的下标
		if (stack.length > 0 && stack[0].temperature > temperatures[i]) {
			let index = stack[0].index;
			ans[i] = index - i;
		}
		// 每次把当前的温度，插入到栈里面去
		// temperature 温度：用于比较温度大小
		// index 与温度对应的下标：用于得出最后的答案
		stack.unshift({ temperature: temperatures[i], index: i });
	}
	console.log(ans);
	return ans
};