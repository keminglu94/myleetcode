/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 * dp[i] 表示 i 金额，所需的最少硬币个数
 * 要明确的一点是，如果存在 coin === amount，那么所需的硬币个数就是 1（最优解）
 * 该问题是寻找最优解，那么 dp[i] 的最优解，应该是 dp[i-coin]+ 1 ，因为 dp[coin] = 1 这是最优解
 * dp方程： dp[i] = Math.min(dp[i], dp[i - coin] + 1);
 * 最后需要注意的两点：
 * 		1. 我们有多个不同面值的硬币，所以需要再多一层循环
 * 		2. 如果我们没有 amount 的解，那么 dp[amount] = Infinity，但是题目需要返回 -1
 */
// 322. 零钱兑换
/**
 * 给你一个整数数组 coins ，表示不同面额的硬币；以及一个整数 amount ，表示总金额。
 * 计算并返回可以凑成总金额所需的 最少的硬币个数 。如果没有任何一种硬币组合能组成总金额，返回 -1 。
 * 你可以认为每种硬币的数量是无限的。
 * 
 * 输入：coins = [1], amount = 0
 * 输出：0
 * 
 * 输入：coins = [2], amount = 3
 * 输出：-1
 */
var coinChange = function (coins, amount) {
	let dp = new Array(amount + 1).fill(Infinity);
	dp[0] = 0;

	for (let i = 1; i <= amount; i++) {
		for (coin of coins) {
			if (coin === i) {
				// 如果有对应的硬币金额，那个数肯定是1个（0除外）
				dp[i] = 1;
				break;
			}
			if (i - coin >= 0) {
				dp[i] = Math.min(dp[i], dp[i - coin] + 1);
			}
		}
	}

	console.log(dp);
	return dp[amount] === Infinity ? -1 : dp[amount];
};