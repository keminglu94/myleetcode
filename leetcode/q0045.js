/**
 * @param {number[]} nums
 * @return {number}
 * 贪心：
 * 如果我们「贪心」地进行正向查找，每次找到可到达的最远位置，就可以在线性时间内得到最少的跳跃次数。
 * 例如，对于数组 [2,3,1,2,4,2,3]，初始位置是下标 0，从下标 0 出发，最远可到达下标 2。
 * 		下标 0 可到达的位置中，下标 1 的值是 3，从下标 1 出发可以达到更远的位置，因此第一步到达下标 1。
 */
// 45. 跳跃游戏 II
/**
 * 给你一个非负整数数组 nums ，你最初位于数组的第一个位置。
 * 数组中的每个元素代表你在该位置可以跳跃的最大长度。
 * 你的目标是使用最少的跳跃次数到达数组的最后一个位置。
 * 假设你总是可以到达数组的最后一个位置。
 */
var jump = function (nums) {
	// end 表示从0开始能达到的最远距离
	// maxIndex 用作 中间值（temp）,得到 下一跳能到达的最远距离
	let ans = 0,end = 0,maxIndex = 0;

	for (let i = 0; i < nums.length - 1; i++) {
		maxIndex = Math.max(maxIndex, nums[i] + i);

		// 表明到了 下一跳的最远距离，然后更新end 和 maxIndex
		if (i === end) {
			end = maxIndex;
			ans++;
		}
	}

	console.log(ans);
	return ans;
};