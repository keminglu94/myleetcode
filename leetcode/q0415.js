/**
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 * 注意：1 + 9 这样，加完之后 还存在进位的情况
 */
// 415. 字符串相加
/**
 * 给定两个字符串形式的非负整数 num1 和num2 ，计算它们的和并同样以字符串形式返回。
 * 你不能使用任何內建的用于处理大整数的库（比如 BigInteger）， 也不能直接将输入的字符串转换为整数形式。
 * 
 * 输入：num1 = "11", num2 = "123"
 * 输出："134"
 * 
 * num1 和num2 都只包含数字 0-9
 * num1 和num2 都不包含任何前导零
 */
var addStrings = function (num1, num2) {
	let ans = "", i = num1.length - 1, j = num2.length - 1, carry = 0;

	while (i >= 0 || j >= 0) {
		let x = i >= 0 ? num1[i] - "0" : 0;
		let y = j >= 0 ? num2[j] - "0" : 0;
		let temp = (x + y + carry) % 10;
		carry = Math.floor((x + y + carry) / 10);
		ans += temp; // js  Number类型与String类型 相加的结果为 String类型
		i--;
		j--;
	}

	if (carry > 0) {
		ans += carry;
	}

	// 站在api的肩膀上
	console.log(ans.split("").reverse().join(""));
	return ans.split("").reverse().join("");
};