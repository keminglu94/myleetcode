/**
 * @param {number[][]} obstacleGrid
 * @return {number}
 * 注意障碍物存在的地方是永远到不了，因此  obstacleGrid[i][j] = 1 时，dp[i][j] = 0
 */
// 63. 不同路径 II
/**
 * 一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为“Start” ）。
 * 机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为“Finish”）。
 * 现在考虑网格中有障碍物。那么从左上角到右下角将会有多少条不同的路径？
 * 
 * 输入：obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]
 * 输出：2
 * 解释：
	3x3 网格的正中间有一个障碍物。
	从左上角到右下角一共有 2 条不同的路径：
	1. 向右 -> 向右 -> 向下 -> 向下
	2. 向下 -> 向下 -> 向右 -> 向右

 * obstacleGrid[i][j] 为 0 或 1
 */
var uniquePathsWithObstacles1 = function (obstacleGrid) {
	let height = obstacleGrid.length, len = obstacleGrid[0].length;

	const dp = Array.from({ length: height }, () => new Array(len).fill(0));

	// 初始化
	let flag = false;
	for (let i = 0; i < height; i++) {
		if (obstacleGrid[i][0] === 1) flag = true;
		dp[i][0] = flag ? 0 : 1;
	}
	flag = false;
	for (let i = 0; i < len; i++) {
		if (obstacleGrid[0][i] === 1) flag = true;
		dp[0][i] = flag ? 0 : 1;
	}

	for (let i = 1; i < height; i++) {
		for (let j = 1; j < len; j++) {
			if (obstacleGrid[i][j] === 0) {
				dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
			} else {
				dp[i][j] = 0
			}
		}
	}

	console.log(dp);
	return dp[height - 1][len - 1];
};
// 优化空间
var uniquePathsWithObstacles2 = function (obstacleGrid) {
	let height = obstacleGrid.length,
		len = obstacleGrid[0].length;

	let dp = new Array(len).fill(0);
	dp[0] = 1; //第一列 暂时用1填充

	for (let i = 0; i < height; i++) {
		for (let j = 0; j < len; j++) {
			if (obstacleGrid[i][j] == 1) {
				//注意条件，遇到障碍物dp[j]就变成0，这里包含了第一列的情况
				dp[j] = 0;
			} else if (j > 0) {
				//只有当j>0 不是第一列了才能取到j - 1
				dp[j] += dp[j - 1];
			}
		}
	}

	console.log(dp);
	return dp[len - 1];
};