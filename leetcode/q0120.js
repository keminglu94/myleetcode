/**
   * @param {number[][]} triangle
   * @return {number}
   * 
   * 1.做题思路，参考p62不同路径这一题的dp思想，我们只要算出到每个位置的最小值，最后在最后一行找到最小值即可。
   * 注意： [[1],[-5,-2],[3,6,1],[-1,2,4,-3]] 这个测试用例
   * 	   在js里面 0 是false，所以判断数字条件应该是 triangle[i - 1][j] === undefined
   */
// 120. 三角形最小路径和
/**
 * 给定一个三角形 triangle ，找出自顶向下的最小路径和。
 * 
 * 每一步只能移动到下一行中相邻的结点上。相邻的结点 在这里指的是 下标 与 上一层结点下标 相同或者等于 上一层结点下标 + 1 的两个结点。也就是说，如果正位于当前行的下标 i ，那么下一步可以移动到下一行的下标 i 或 i + 1 。
 * 
 * 示例1:
 * 
 * 输入：triangle = [[2],[3,4],[6,5,7],[4,1,8,3]]
 * 输出：11
 * 解释：如下面简图所示：
2
3 4
6 5 7
4 1 8 3
自顶向下的最小路径和为 11（即，2 + 3 + 5 + 1 = 11）。
 * 
 */

// 1.从上往下 遍历
var minimumTotal1 = function (triangle) {
	let n = triangle.length

	for (let i = 1; i < n; i++) {
		for (let j = 0; j < triangle[i].length; j++) {
			triangle[i][j] += Math.min(
				(triangle[i - 1][j] === undefined ? Infinity : triangle[i - 1][j]),
				(triangle[i - 1][j - 1] === undefined ? Infinity : triangle[i - 1][j - 1]),
			)
		}
	}

	console.log(triangle)

	let ans = Infinity

	triangle[n - 1].forEach(item => {
		if (ans > item) {
			ans = item
		}
	})

	return ans
};


// 2.从下往上 遍历
var minimumTotal2 = function (triangle) {
	let n = triangle.length

	// 跟第一种方法 反着来
	for (let i = n - 2; i >= 0; i--) {
		for (let j = 0; j < triangle[i].length; j++) {
			triangle[i][j] += Math.min(
				triangle[i + 1][j] === undefined ? Infinity : triangle[i + 1][j],
				triangle[i + 1][j + 1] === undefined ? Infinity : triangle[i + 1][j + 1],
			)
		}
	}

	console.log(triangle[0][0]);

	return triangle[0][0]
}