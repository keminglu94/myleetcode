/**
 * @param {number} n 
 * @returns {number}
 * 斐波那契数列 F(0)=1，F(1)=1, F(n)=F(n - 1)+F(n - 2)（n ≥ 2，n ∈ N*）
 */
// 70. 爬楼梯
var climbStairs = function (n) {
	if (n === 1) return 1;
	if (n === 2) return 2;
	let x = 1, y = 2, ans = 3
	for (let i = 3; i < n; i++) {
		x = y
		y = ans
		ans = x + y
	}
	return ans
};