/**
 * @param {number[]} nums
 * @return {number[][]}
 * 经典回溯算法
 */
// 78. 子集
/**
 * 给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。
 * 解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。
 * 
 * 输入：nums = [1,2,3]
 * 输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
 */
var subsets = function (nums) {
	let ans = [];

	// dfs函数
	const dfs = (path, start) => {
		// 超过数组长度时，跳出循环
		if (start > nums.length) return;
		ans.push([...path]);

		for (let i = start; i < nums.length; i++) {
			path.push(nums[i]);

			// i+1 下一个状态
			dfs(path, i + 1);

			path.pop();
		}
	};

	dfs([], 0);

	console.log(ans);
	return ans;
};