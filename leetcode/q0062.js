/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
// 62. 不同路径
/**
 * 一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。
 * 机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
 * 问总共有多少条不同的路径
 */
var uniquePaths1 = function (m, n) {
	// 防止js数据引用
	const dp = Array.from({length: m},()=>new Array(n).fill(1));
	// const dp = new Array(m).fill(0).map(() => new Array(n).fill(1));

	for (let i = 1; i < m; i++) {
		for (let j = 1; j < n; j++) {
			dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
		}
	}

	console.log(dp);
	return dp[m - 1][n - 1]
};
// 优化空间
var uniquePaths2 = function (m, n) {
	let dp = new Array(n).fill(1)

	for (let i = 1; i < m; i++) {
		for (let j = 1; j < n; j++) {
			dp[j] += dp[j - 1]
		}
	}

	console.log(dp);
	return dp[n - 1]
};