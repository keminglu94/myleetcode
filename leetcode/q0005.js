/**
 * @param {string} s
 * @return {string}
 *  如果 s[i]——s[j] 是一个回文串，那么 s[i+1]——s[j-1] 必定是一个回文串
 */
// 力扣 最长回文子串 Q5
// 给你一个字符串 s，找到 s 中最长的回文子串。
var longestPalindrome = function (s) {
	function Palindrome(s, left, right) {
		while (left >= 0 && right < s.length && s[left] === s[right]) {
			left--;
			right++;
		}
		return s.slice(left + 1, right);
	}
	let ans = "";
	for (let i = 0; i < s.length; i++) {
		let s1 = Palindrome(s, i, i);		// 单数回文
		let s2 = Palindrome(s, i, i + 1);	// 双数回文
		ans = ans.length > s1.length ? ans : s1;
		ans = ans.length > s2.length ? ans : s2;
	}
	return ans;
};