/**
 * @param {number[]} cost
 * @return {number}
 * 1.假设数组 cost 的长度为 n，则 n 个阶梯分别对应下标 0 到 n-1，楼层顶部对应下标 n
 * 问题等价于计算达到下标 n 的最小花费。
 * 2.创建长度为 n+1 的数组dp，dp[i]表示到达 下标i的最小花费
 * 3.初始化 dp[0]=dp[1]=0，因为刚开始是可以选择下标 0或1 作为初始阶梯
 * 4.当 2<=i<=n 时，到达下标有两个方法：
 * 		a. 从下标 i-1 使用 cost[i-1] 费用
 * 		b. 从下标 i-2 使用 cost[i-2] 费用
 * 可以得出到达下标i的费用：
 * 		a.	dp[i-1]+cost[i-1]
 * 		b.  dp[i-2]+cost[i-2]
 * 最后得出状态转移方程：
 * 	dp[i] = Math.min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2])
 * 
 * 5.总结：题目的要求是 到达下标i ，就找到达下标i有几种方法，然后从这几种方法中找到花费最小值（或者最大值）。
 */
// 746. 使用最小花费爬楼梯
/**
 * 给你一个整数数组 cost ，其中 cost[i] 是从楼梯第 i 个台阶向上爬需要支付的费用。一旦你支付此费用，即可选择向上爬一个或者两个台阶。
 * 你可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯。
 * 请你计算并返回达到楼梯顶部的最低花费。
 * 
 *输入：cost = [1,100,1,1,1,100,1,1,100,1]
 *输出：6
 *解释：你将从下标为 0 的台阶开始。
- 支付 1 ，向上爬两个台阶，到达下标为 2 的台阶。
- 支付 1 ，向上爬两个台阶，到达下标为 4 的台阶。
- 支付 1 ，向上爬两个台阶，到达下标为 6 的台阶。
- 支付 1 ，向上爬一个台阶，到达下标为 7 的台阶。
- 支付 1 ，向上爬两个台阶，到达下标为 9 的台阶。
- 支付 1 ，向上爬一个台阶，到达楼梯顶部。
总花费为 6 。

 * 2 <= cost.length <= 1000
 * 0 <= cost[i] <= 999
 */
var minCostClimbingStairs1 = function (cost) {
	const len = cost.length;
	const dp = new Array(len + 1);
	dp[0] = dp[1] = 0;

	for (let i = 2; i <= len; i++) {
		dp[i] = Math.min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
	}

	console.log(dp[len]);
	return dp[len];
};

// 空间优化：在原数组上修改 （题目没说不能修改原数组）
// 注意：我们要到达 下标n 而不是下标 n-1或者n-2 
// 但是对于下标n，是不需要费用的，所以最后还需要判断 cost[n - 1]和cost[n - 2]的大小
var minCostClimbingStairs2 = function (cost) {
	let n = cost.length;
	for (let i = 2; i < n; i++) {
		cost[i] += Math.min(cost[i - 1], cost[i - 2]);
	}
	console.log(Math.min(cost[n - 1], cost[n - 2]));
	return Math.min(cost[n - 1], cost[n - 2]);
};

// 空间优化：pre 之前的一步，down 下一步
var minCostClimbingStairs3 = function (cost) {
	let pre = cost[0], down = cost[1];
	for (let i = 2; i < cost.length; i++) {
		// 从 i-1 或者 i-2 出发，才能到达下标 i 
		let temp = cost[i] + Math.min(pre, down);

		pre = down;

		down = temp;
	}

	console.log(Math.min(pre, down));
	return Math.min(pre, down);
};
