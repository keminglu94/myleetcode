/**
 * @param {number[]} nums
 * @return {number}
 * 有环的最大自序和 = 数组总和 - 最小自序和
 * 同时参照 53. 最大子数组和
 */
// 918. 环形子数组的最大和
// 子数组的长度一定要大于0，即子数组至少存在一个元素
var maxSubarraySumCircular = function (nums) {

	let max = nums[0],min = nums[0],sum = nums[0];
	let maxTemp = nums[0],minTemp = nums[0];

	for (let i = 1; i < nums.length; i++) {

		if (maxTemp < 0) {
			maxTemp = nums[i];
		} else {
			maxTemp += nums[i];
		}

		if (minTemp > 0) {
			minTemp = nums[i];
		} else {
			minTemp += nums[i];
		}

		max = Math.max(maxTemp, max);
		min = Math.min(minTemp, min);
		sum += nums[i];
	}

	//  sum - min === 0 数组全是负数的情况，因此我们只要取 max 即可
	return sum - min === 0 ? max : Math.max(max, sum - min)
};
