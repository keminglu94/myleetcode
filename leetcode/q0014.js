/**
 * @param {string[]} strs
 * @return {string}
 */
// 14. 最长公共前缀
/**
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 * 如果不存在公共前缀，返回空字符串 ""。
 * 
 * 1 <= strs.length <= 200
 * 0 <= strs[i].length <= 200
 */
var longestCommonPrefix = function (strs) {
	if (strs.length === 0) return "";
	let min = Infinity; // 表示正无穷大的数值。

	// 找到最短的字符串长度，公共前缀肯定不会超过最短的字符串长度
	strs.forEach((item) => {
		if (item.length < min) min = item.length;
	});

	let doing = true; // 判断是否继续寻找 最长公共前缀
	while (doing) {
		// flag 表示是否找到了最长公共前缀
		let temp = strs[0].slice(0, min), flag = false;
		for (let i = 1; i < strs.length; i++) {
			if (strs[i].slice(0, min) !== temp) {
				min--;
				flag = true;
				break;
			}
		}
		doing = flag;
	}

	console.log(strs[0].slice(0, min));
	return strs[0].slice(0, min);
};