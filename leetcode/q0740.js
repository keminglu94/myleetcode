/**
 * @param {number[]} nums
 * @return {number}
 * 变相的 打家劫舍
 * 拿到 nums[i] 之后在删除 nums[i-1] 和 nums[i+1] ,类似于不能偷两间相同的房屋
 * 那么我们的问题就是 把 nums 变成打家劫舍的 robList 
 * 例如： nums = [1, 2, 2, 3, 5, 6, 6, 7, 8]  转化成 robList = [0, 1, 4, 3, 0, 5, 12, 7, 8]
 * 解释 robList 下标 0 对应nums中数字 0 的和； 下标 1 对应nums中 数字 1 的和为1（nums中有一个1）； 下标2 对应nums中 数字 2和为4（ nums有两个 2 ）
 * 下标4 对应nums中 数字 4 的和 为0（因为nums中没有4）
 */
// 740. 删除并获得点数
/**
 * 给你一个整数数组 nums ，你可以对它进行一些操作。
 * 每次操作中，选择任意一个 nums[i] ，删除它并获得 nums[i] 的点数。之后，你必须删除 所有 等于 nums[i] - 1 和 nums[i] + 1 的元素。
 * 开始你拥有 0 个点数。返回你能通过这些操作获得的最大点数。
 * 
 * 输入：nums = [3,4,2]
 * 输出：6
 * 解释：删除 4 获得 4 个点数，因此 3 也被删除。之后，删除 2 获得 2 个点数。总共获得 6 个点数。
 * 
 * 1 <= nums.length <= 2 * 10^4
 * 1 <= nums[i] <= 10^4
 */
var deleteAndEarn = function (nums) {
	const rob = (nums) => {
		// nums[i] >=1 ，那么 robList.length >= 2
		// if (nums.length === 1) return nums[0]; 
		const size = nums.length;
		let first = nums[0],
			second = Math.max(nums[0], nums[1]);
		for (let i = 2; i < size; i++) {
			let temp = Math.max(first + nums[i], second);
			first = second;
			second = temp;
		}
		return second;
	};

	// 拿到最大值，得出robList的边界
	let maxVal = 0;
	nums.forEach((num) => {
		maxVal = maxVal > num ? maxVal : num;
	});

	// 转化成 robList数组
	let robList = new Array(maxVal + 1).fill(0);
	nums.forEach((num) => {
		robList[num] += num;	// 核心
	});

	console.log("robList", robList);

	return rob(robList);
};
