/**
 * @param {number[]} nums
 * @return {boolean}
 * 
 * 题意： 假设nums的长度为n	得出	dp[i] = Math.max(nums[i] + i, dp[i - 1]), 最后判断 dp[n-2] >= n-1 ,但是需要前提就是 dp[i - 1] >= i，一定要能到达下一步
 * 
 * 这题感觉放在贪心更好
 * 
 * 给定一个非负整数数组 nums ，你最初位于数组的 第一个下标 。
 * 数组中的每个元素代表你在该位置可以跳跃的最大长度。
 * 判断你是否能够到达最后一个下标。
 * 
 * 输入：nums = [3,2,1,0,4]
 * 输出：false
 * 解释：无论怎样，总会到达下标为 3 的位置。但该下标的最大跳跃长度是 0 ， 所以永远不可能到达最后一个下标。
 * 
 * 1 <= nums.length <= 3 * 10^4
 * 0 <= nums[i] <= 105
 */
var canJump1 = function (nums) {
	if (nums[0] >= nums.length - 1) return true;

	let n = nums.length;
	let dp = new Array(n);
	dp[0] = nums[0];

	for (let i = 1; i < n - 1; i++) {
		if (dp[i - 1] < i) return false;
		if (dp[i] + i >= n - 1) return true;
		dp[i] = Math.max(nums[i] + i, dp[i - 1]);
	}

	return dp[n - 2] >= n - 1;
};
// 贪心
var canJump2 = function (nums) {
	if (nums[0] >= nums.length - 1) return true;
	let curMaxLen = nums[0];

	for (let i = 1; i < nums.length - 1; i++) {
		if (curMaxLen < i) return false; // 连下一步都到不了，肯定到不了终点
		curMaxLen = Math.max(curMaxLen, nums[i] + i); // 拿到从0出发 能到达的最远下标
		if (curMaxLen >= nums.length - 1) return true;	// 如果能到达nums的最后一个元素 直接返回true
	}
	// 循环跑完了 没有return 表明到不了 nums.length-2 , 那肯定也到不了 nums.length - 1
	return false;
};
