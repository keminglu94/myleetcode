/**
 * @param {number[]} nums
 * @return {number}
 */
// 136. 只出现一次的数字
// 给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
var singleNumber1 = function (nums) {
	let obj = {}, ans;
	for (num of nums) {
		if (obj[num]) {
			obj[num]++;
		} else {
			obj[num] = 1;
		}
	}
	Object.keys(obj).forEach((key) => {
		if (obj[key] === 1) {
			ans = key;
		}
	});
	console.log(ans);
	return ans;
};

var singleNumber2 = function (nums) {
	let ans = 0;
	for (num of nums) {
		ans ^= num
	}
	return ans
};