/*
	1.js函数柯里化
	add.prototype = {
		constructor: {
			length:3,
			name:"add",
			...........
		}
	}
	// 1.原型链原理 找到length
	// 2.length 长度就是add 函数定义时 参数的个数
	add.length = add.prototype.constructor.length = 3
*/
function add(a, b, c) {
	return a + b + c
}

// 柯里化
// Array.prototype.slice.call 可以把类数组转化成数组
function curry(func) {
	// 类型判断可以不加
	if (typeof func !== 'function') {
		throw TypeError('func must be function')
	}
	const len = func.length

	// 柯里化 肯定是返回一个函数；然后内部在进行处理，处理到最后一个参数的时候在得到结果。
	return function reply() {
		var innerLen = arguments.length, args = Array.prototype.slice.call(arguments);
		if (innerLen >= len) {
			// 处理到最后一个参数，直接调用原函数方法拿到结果
			return func.apply(undefined, args)
		} else {
			return function () {
				var innerArg = Array.prototype.slice.call(arguments), allArgs = args.concat(innerArg)
				return reply.apply(undefined, allArgs)
			}
		}
	}
}

// 简洁版，使用 ... 可以转化成数组
function curry1(func) {
	return function reply(...finalArg) {

		if (finalArg.length >= func.length) {
			return func.apply(this, finalArg)

		} else {

			return function (...tempArg) {
				return reply.apply(this, finalArg.concat(tempArg))
			}
		}
	}
}

const _fn = curry(add)
console.log('curry--', _fn(1)(2)(3), 'add--', add(1, 2, 3));

/**
 * 2.简单queryString
 * 简单实现一个 queryString 具有 parse 和 stringify
 * parse用于把一个URL查询字符串解析成一个健值对集合
 * 输入 'foo=bar&abc=1&abc=2&abc=3&abc=4&abc=5'
 * 输出 
 * {
		foo: 'bar',
		abc: ['1', '2', '3', '4', '5'],
	}

	stringify 相反的，用于序列化对象自身属性，生成URL 查询字符串
	输入 
	{
		foo: 'bar',
		abc: ['1', '2', '3', '4', '5'],
	}
	输出 'foo=bar&abc=1&abc=2&abc=3&abc=4&abc=5'
 */

let a = {
	foo: 'bar',
	abc: ['1', '2', '3', '4', '5'],
	name: 'ake',
}

let b = 'foo=bar&abc=1&abc=2&abc=3&abc=4&abc=5'

function stringify(params) {
	let ans = []

	Object.keys(params).forEach(key => {
		let value = params[key]

		if (value instanceof Array) {
			value.forEach(item => {
				ans.push(`${key}=${item}`)
			})
		} else {
			ans.push(`${key}=${value}`)
		}
	})

	return ans.join('&')
}

// console.log(stringify(a));

function parse(params = '') {
	let data = params.split('&')

	return data.reduce((pre, cur) => {
		let [key, value] = cur.split('=')

		if (Object.hasOwn(pre, key)) {
			let keyValue = pre[key]

			if (typeof keyValue === 'object') {
				pre[key].push(value)
			} else {
				pre[key] = [pre[key], value]
			}

		} else {
			pre[key] = value
		}
		return pre
	}, {})
}

// console.log(parse(stringify(a)));

function queryString() {
	this.parse = parse

	this.stringify = stringify
}

const test = new queryString()

console.log(test.parse(b));
console.log(test.stringify(a));

/**
 * 3.对象扁平化
 * 输入：
 * var input = {
		a: 1,
		b: [1, 2, { c: true }, [3]],
		d: { e: 2, f: 3 },
		g: null,
	}
	输出：
	{
		"a": 1,
		"b[0]": 1,
		"b[1]": 2,
		"b[2].c": true,
		"b[3][0]": 3,
		"d.e": 2,
		"d.f": 3
	}
	// 本题需要舍弃 null 和 undefined ，因此 "g" 没有在输出里
 */
var input = {
	a: 1,
	b: [1, 2, { c: true }, [3]],
	d: { e: 2, f: 3 },
	g: null,
}

function flatten(params, preKey = "", res = {}, isArray = false) {
	for (let [curKey, value] of Object.entries(params)) {
		if (value instanceof Array) {
			let tmp = isArray ? `${preKey}[${curKey}]` : preKey + curKey
			flatten(value, tmp, res, true)
		} else if (value instanceof Object) {
			let tmp = isArray ? `${preKey}[${curKey}].` : preKey + curKey + "."
			flatten(value, tmp, res, false)
		} else if (value !== null && value !== undefined) {
			let tmp = isArray ? `${preKey}[${curKey}]` : preKey + curKey
			res[tmp] = value
		}
	}
	return res
}

console.log(flatten(input));

/*
	4.问题：在不支持es6数组的 reduce 方法的浏览器下，执行一段代码，让后续初始化的数组都有一个reduce方法。
*/
// 从手写 reduce 可以看出 
// 当我们调用 reduce 只给了callback时 reduce只循环 length-1，第一次不进入循环（因为要给 initData赋值）
Array.prototype.myReduce = function (callback, initData) {

	if (typeof callback !== 'function') {
		throw new TypeError(callback + ' is not a function')
	}

	let pre, index;
	if (initData !== undefined) {
		pre = initData
		index = 0
	} else {
		pre = this[0]
		index = 1
	}

	for (let i = index; i < this.length; i++) {
		pre = callback(pre, this[i], i)
	}

	return pre

}

Array.prototype.myReduce = function (callback, initData) {

	if (typeof callback !== 'function') {
		throw new TypeError(callback + ' is not a function')
	}

	this.forEach((item, index) => {
		if (index == 0 && initData == undefined) {
			initData = item
		} else {
			initData = callback(initData, item, index)
		}
	})

	return initData
}

/*
5.问题：
/**in
 * 请实现一个增强版本的 Promise.all，支持数组与对象方式。
 * - 如果传入的是数组形式的 promises，则返回结果数组；
 * - 如果传入 {a: Promise} 则返回对象。
 * @param {Array|Object} promises
 * @return {Promise<any>} 新的 Promise 对象
 *
 * 例如：
 * 输入：{ a: Promise.resolve(1), b: Promise.resolve(2)}
 * 返回：Promise.resolve({a: 1, b: 2})
 */

Promise.prototype.myAll = function (promises) {
	let arr = [], res = {};
	let i = 0;
	function processData(index, data, isArray) {
		if (isArray) {
			arr[index] = data;
			i++;
			if (i == promises.length) {
				resolve(arr);
			};
		} else {
			res[index] = data;
			i++;
			if (i == Object.keys(promises).length) {
				resolve(res);
			};
		}
	};

	return new Promise((resolve, reject) => {
		if (promises instanceof Array) {
			for (let i = 0; i < promises.length; i++) {
				promises[i].then(data => {
					processData(i, data, true);
				}, reject);
			};
		} else {
			Object.keys(promises).forEach(key => {
				promises[key].then(data => {
					processData(key, data, false);
				}, reject)
			})
		}
	});
}

/**
 * 6.手写filter
 */
Array.prototype.myFilter = function (callback) {
	let res = []

	for (let i = 0; i < this.length; i++) {
		if (callback(this[i], i)) {
			res.push(this[i])
		}
	}

	return res
}

/**
 * 7.手写call apply bind
 */
// call 改变函数 this的指向，并返回函数执行的结果，参数是一个个传递的
Function.prototype.myCall = function (context, ...args) {
	// const [context, ...args] = Array.from(arguments)
	const func = this

	context[func] = func

	const res = context[func](...args)

	delete context[func]

	return res
}

// apply 改变函数 this的指向，并返回函数执行的结果，参数是数组传递
Function.prototype.myApply = function (context, args) {
	const func = this

	context[func] = func

	const res = context[func](...args)

	delete context[func]

	return res
}

// bind 改变函数 this的指向，并返回函数（不执行），参数是一个个传递的
Function.prototype.myBind = function (context, ...args) {
	const func = this

	return function f() {
		// 考虑 bind 后返回的函数作为构造函数被 new
		if (this instanceof f) {
			return new func(...args, ...arguments)
		}
		return func.apply(context, [...args, ...arguments])
	}
}

// 简易版bind
Function.prototype.fakeBind = function (obj, ...args) {
	return (...rest) => this.call(obj, ...args, ...rest);
};

// 《JavaScript 权威指南》P191 ES3 实现 bind
if (!Function.prototype.bind) {
	Function.prototype.bind = function (context) {
		var self = this, boundArgs = arguments;
		return function () {
			var i, args = [];
			for (i = 1; i < boundArgs.length; i++) {
				args.push(boundArgs[i])
			}
			for (i = 0; i < arguments.length; i++) {
				args.push(arguments[i])
			}
			return self.apply(context, args)
		}
	}
}

/**
 * 8.实现一个无限累加的 sum 函数
 * 
 * sum(1, 2, 3).valueOf(); //6
 * sum(2, 3)(2).valueOf(); //7
 * sum(1)(2)(3)(4).valueOf(); //10
 * sum(2)(4, 1)(2).valueOf(); //9
 * sum(1)(2)(3)(4)(5)(6).valueOf(); // 21
 */
function sum(...arg) {
	const f = (...rest) => sum(...arg, ...rest)
	f.valueOf = () => arg.reduce((pre, cur) => pre + cur)
	return f
}

/** 
 * 9.为何 0.1+0.2 不等于 0.3，应如何做相等比较
 * 0.1，0.2 表示为二进制会有精度的损失
 * 比较时可引入一个很小的数值 Number.EPSILON 容忍误差，其值为 2^-52。
*/

function Equal(a, b) {
	return Math.abs(a - b) < Number.EPSILON;
}

/** 
 * 10. vue2 数据劫持
 * 
*/
function observe(data) {
	for (let key in data) {
		let value = data[key];

		if (value && typeof value === "object") {
			observe(value);
		}

		Object.defineProperty(data, key, {
			get() {
				return value;
			},
			set(newValue) {
				if (value === newValue) {
					return
				}
				console.log('observe update');
				value = newValue;
			}
		})
	}
}
/** 
 * 11.发布-订阅模式
*/
class EventCenter {
	// 事件列表
	eventList = {};

	// 订阅事件
	subscribeEvent(type, event) {
		if (!this.eventList[type]) {
			this.eventList[type] = [];
		}
		this.eventList[type].push(event)
	}

	// 发布事件
	dispatchEvent(type, ...params) {
		if (!this.eventList[type]) {
			throw Error(`无效事件`)
		}
		this.eventList[type].forEach(handler => {
			handler(...params)
		})
	}

	// 移除事件
	removeEvent(type, handler) {
		// 无效事件抛出
		if (!(type in this.eventList)) {
			throw Error('无效事件')
		}
		if (!handler) {
			delete this.eventList[type]
		} else {
			const index = this.eventList[type].findIndex(ele => ele === handler)
			if (index <= 0) {
				throw Error('无效事件')
			}
			this.eventList[type].splice(index, 1)
			if (this.eventList[type].length === 0) {
				delete this.eventList[type]
			}
		}
	}
}

// 测试代码
// 定义一个自定义事件 load
function load(params) {
	console.log('load---', params)
}

function load2(params) {
	console.log('load2---', params)
}

const eventCenter = new EventCenter() // 创建event实例

eventCenter.subscribeEvent('load', load)

eventCenter.subscribeEvent('load', load2)

// 触发该事件
eventCenter.dispatchEvent('load', 'load事件触发')
// 移除load2事件
eventCenter.removeEvent('load', load2)
// 移除所有load事件
eventCenter.removeEvent('load')