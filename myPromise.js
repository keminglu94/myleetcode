/**
 * 
 * 原生promise的基本使用
const test = new Promise((resolve, reject) => {
    resolve('success')
    reject('err')
})

test.then(value => {
    console.log('resolve', value)
}, reason => {
    console.log('reject', reason)
})

1.Promise 是一个类，在执行这个类的时候会传入一个执行器，这个执行器会立即执行
2.Promise 会有三种状态
    Pending 等待
    Fulfilled 完成
    Rejected 失败

3.状态只能由 Pending --> Fulfilled 或者 Pending --> Rejected，且一但发生改变便不可二次修改；
4.Promise 中使用 resolve 和 reject 两个函数来更改状态；
5.then 方法内部做但事情就是状态判断
    如果状态是成功，调用成功回调函数
    如果状态是失败，调用失败回调函数
 */


/**
 * 个人总结：
 * 1.resolve和reject 都只能在 PENDING 下才能执行，同时修改状态和值，因为promise的状态只能确定一次
 * 2.异步处理、then方法多次调用，需要callbacks数组去存储 resolve和reject 的回调函数，然后在 resolve和reject中分别调用各自callbacks中的回调函数
 * 3.then方法支持链式调用 所以需要返回promise对象。
 *  3.1 如果没有一直链式调用，就要resolve(value)
 *  3.2 如果在链式调用，就要直接用 promise.then(resolve,reject)
 *  3.3 如果返回的promise是自身，那么就直接报错 reject(new TypeError('Chaining cycle detected for promise #<Promise>'))
 */

// 先定义三个常量表示状态
const PENDING = 'pending';
const FULFILLED = 'fulfilled';
const REJECTED = 'rejected';

// 新建 MyPromise 类
class MyPromise {
    constructor(executor) {
        // executor 是一个执行器，进入会立即执行
        // 并传入resolve和reject方法
        try {
            executor(this.resolve, this.reject)
        } catch (error) {
            this.reject(error)
        }
    }

    // 储存状态的变量，初始值是 pending
    status = PENDING;
    // 成功之后的值
    value = null;
    // 失败之后的原因
    reason = null;

    // 存储成功回调函数
    onFulfilledCallbacks = [];
    // 存储失败回调函数
    onRejectedCallbacks = [];

    // 更改成功后的状态
    resolve = (value) => {
        // 只有状态是等待，才执行状态修改
        if (this.status === PENDING) {
            // 状态修改为成功
            this.status = FULFILLED;
            // 保存成功之后的值
            this.value = value;
            // resolve里面将所有成功的回调拿出来执行
            while (this.onFulfilledCallbacks.length) {
                // Array.shift() 取出数组第一个元素，然后（）调用，shift不是纯函数，取出后，数组将失去该元素，直到数组为空
                this.onFulfilledCallbacks.shift()(value)
            }
        }
    }

    // 更改失败后的状态
    reject = (reason) => {
        // 只有状态是等待，才执行状态修改
        if (this.status === PENDING) {
            // 状态成功为失败
            this.status = REJECTED;
            // 保存失败后的原因
            this.reason = reason;
            // resolve里面将所有失败的回调拿出来执行
            while (this.onRejectedCallbacks.length) {
                this.onRejectedCallbacks.shift()(reason)
            }
        }
    }

    then(onFulfilled, onRejected) {
        const realOnFulfilled = typeof onFulfilled === 'function' ? onFulfilled : value => value;
        const realOnRejected = typeof onRejected === 'function' ? onRejected : reason => { throw reason };

        // 为了链式调用这里直接创建一个 MyPromise，并在后面 return 出去
        const promise2 = new MyPromise((resolve, reject) => {
            const fulfilledMicrotask = () => {
                // 创建一个微任务等待 promise2 完成初始化
                queueMicrotask(() => {
                    try {
                        // 获取成功回调函数的执行结果
                        const x = realOnFulfilled(this.value);
                        // 传入 resolvePromise 集中处理
                        resolvePromise(promise2, x, resolve, reject);
                    } catch (error) {
                        reject(error)
                    }
                })
            }

            const rejectedMicrotask = () => {
                // 创建一个微任务等待 promise2 完成初始化
                queueMicrotask(() => {
                    try {
                        // 调用失败回调，并且把原因返回
                        const x = realOnRejected(this.reason);
                        // 传入 resolvePromise 集中处理
                        resolvePromise(promise2, x, resolve, reject);
                    } catch (error) {
                        reject(error)
                    }
                })
            }
            // 判断状态
            if (this.status === FULFILLED) {
                fulfilledMicrotask()
            } else if (this.status === REJECTED) {
                rejectedMicrotask()
            } else if (this.status === PENDING) {
                // 等待
                // 因为不知道后面状态的变化情况，所以将成功回调和失败回调存储起来
                // 等到执行成功失败函数的时候再传递
                this.onFulfilledCallbacks.push(fulfilledMicrotask);
                this.onRejectedCallbacks.push(rejectedMicrotask);
            }
        })

        return promise2;
    }

    // resolve 静态方法
    static resolve(parameter) {
        // 如果传入 MyPromise 就直接返回
        if (parameter instanceof MyPromise) {
            return parameter;
        }

        // 转成常规方式
        return new MyPromise(resolve => {
            resolve(parameter);
        });
    }

    // reject 静态方法
    static reject(reason) {
        return new MyPromise((resolve, reject) => {
            reject(reason);
        });
    }
}

function resolvePromise(promise2, x, resolve, reject) {
    // 如果相等了，说明return的是自己，抛出类型错误并返回
    if (promise2 === x) {
        return reject(new TypeError('Chaining cycle detected for promise #<Promise>'))
    }
    // 判断x是不是 MyPromise 实例对象
    if (x instanceof MyPromise) {
        // 执行 x，调用 then 方法，目的是将其状态变为 fulfilled 或者 rejected
        // x.then(value => resolve(value), reason => reject(reason))
        // 简化之后
        x.then(resolve, reject)
    } else {
        // 普通值
        resolve(x)
    }
}

// module.exports = MyPromise;

const a = new MyPromise((resolve, reject) => {
    reject('a-reject')
    // resolve('a-resolve')

    // setTimeout(() => {
    //     resolve('a-resolve')
    // }, 1000)

});

a.then(res => {
    console.log('a res', res);
},)



// promise的各种方法

// 接收一个Promise数组，数组中如有非Promise项，则此项当做成功
// 如果所有Promise都成功，则返回成功结果数组
// 如果有一个Promise失败，则返回这个失败结果
Promise.prototype.myAll = function (promiseList) {
    let resultList = [], cnt = 0

    return new Promise((resolve, rejected) => {

        const addResult = (index, value) => {
            resultList[index] = value
            cnt++

            if (cnt === promiseList.length) {
                resolve(resultList)
            }
        }

        promiseList.forEach((promise, index) => {
            if (promise instanceof Promise) {
                promise.then(res => {
                    addResult(index, res)
                }).catch(err => {
                    rejected(err)
                })
            } else {
                addResult(index, promise)
            }
        });
    })

}

// 接收一个Promise数组，数组中如有非Promise项，则此项当做成功
// 哪个Promise最快得到结果，就返回那个结果，无论成功失败
Promise.prototype.myRace = function (promiseList) {
    return new Promise((resolve, rejected) => {
        promiseList.forEach(promise => {
            if (promise instanceof Promise) {
                promise.then(res => {
                    resolve(res)
                }).catch(err => {
                    rejected(err)
                })
            }else{
                resolve(promise)
            }
        })
    })
}

// 接收一个Promise数组，数组中如有非Promise项，则此项当做成功
// 把每一个Promise的结果，集合成数组，返回
Promise.prototype.myAllSettled = function (promiseList) {

    let resultList = [], cnt = 0

    return new Promise((resolve, rejected) => {
        const addResult = (index, value) => {
            resultList[index] = value
            cnt++
            if (cnt === promiseList.length) {
                resolve(resultList)
            }
        }

        promiseList.forEach((promise, index) => {
            if (promise instanceof Promise) {
                promise.then(res => {
                    addResult(index, res)
                }).catch(err => {
                    addResult(index, err)
                })
            } else {
                addResult(index, promise)
            }
        })
    })
}

// 接收一个Promise数组，数组中如有非Promise项，则此项当做成功
// 如果有一个Promise成功，则返回这个成功结果
// 如果所有Promise都失败，则报错
Promise.prototype.myAny = function (promiseList) {
    let cnt = 0

    return new Promise((resolve, rejected) => {
        promiseList.forEach(promise => {
            if (promise instanceof Promise) {
                promise.then(res => {
                    resolve(res)
                }).catch(err => {
                    cnt++
                    if (cnt === promiseList.length) {
                        rejected('All promises were rejected');
                    }
                })
            } else {
                resolve(promise)
            }
        })
    })
}

